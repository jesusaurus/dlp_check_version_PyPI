Script to detect needed version updates in dlpy_ and subpkgs from 
PyPI_.

.. _dlpy:
    https://build.opensuse.org/project/show/devel:languages:python

.. _PyPI:
    https://pypi.org/
