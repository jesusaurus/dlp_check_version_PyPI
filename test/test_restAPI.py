import os.path
import unittest
from distutils.version import LooseVersion
from unittest.mock import patch, MagicMock

import dlpcvp


class TestUrlopen(unittest.TestCase):
    @patch('dlpcvp.urllib.request.urlopen')
    def test_get_version_called_url(self, mock_obj):
        result = dlpcvp.get_version_from_pypi('m2crypto')
        url = dlpcvp.PyPI_base.format('m2crypto')
        req_obj = mock_obj.call_args[0][0]
        self.assertEqual(req_obj.full_url, url)
        mock_obj.assert_called_once()

    @unittest.skip('Not done yet')
    @patch('dlpcvp.urllib.request.urlopen')
    def test_get_version_returned(self, mock_obj):
        mock_obj.read.return_value = {
            'info': {
                    "name": "M2CryptoTest",
                    "version": '42'
            }
        }
        mock_obj.info.return_value = 'AAA'
        expected = ('M2CryptoTest', '0.30.1', 'AAA')

        result = dlpcvp.get_version_from_pypi('m2crypto')
        self.assertEqual(result, expected)
        mock_obj.assert_called_once()


class TestSpecParser(unittest.TestCase):
    def test_parse_spec_file(self):
        spec_fn = os.path.join(os.path.dirname(__file__), 'emacs.spec')
        with open(spec_fn) as in_file:
            ver = dlpcvp.parse_spec(in_file.read(), 'emacs')
        self.assertEqual(ver, LooseVersion('26.1'))
